# Change Log

## [Unreleased]

## [1.0.7] - 2020-08-28 01:09:14

### Fixed

- fixed minor bugs

---

## [Hint]

| type       | description                            |
| ---------- | -------------------------------------- |
| Added      | for new features.                      |
| Changed    | for changes in existing functionality. |
| Deprecated | for soon-to-be removed features.       |
| Removed    | for now removed features.              |
| Fixed      | for any bug fixes.                     |
| Security   | in case of vulnerabilities.            |
